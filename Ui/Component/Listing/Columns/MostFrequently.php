<?php
/**
 * Created by PhpStorm.
 * User: duccanh
 * Date: 15/07/2016
 * Time: 11:06
 */
namespace Magenest\FAQProfessional\Ui\Component\Listing\Columns;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class MostFrequently
 *
 * @package Magenest\FAQProfessional\Ui\Component\Listing\Columns
 */
class MostFrequently extends Column
{
    /**
     *  MostFrequently
     */
    const YES = 1;
    const NO = 2;

    /**
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    )
    {
        parent::__construct($context, $uiComponentFactory, $components, $data);

    }

    /**
     * Prepare Data Source
     *
     * @param  array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                if (!empty($item['most_frequently'])) {
                    $item['most_frequently'] = strip_tags($this->getOptionGrid($item['most_frequently']), ENT_IGNORE);
                }
            }
        }

        return $dataSource;

    }


    /**
     * @return array
     */
    public static function getOptionArray()
    {
        return [
            self::YES => __('Yes'),
            self::NO => __('No'),
        ];

    }


    /**
     * @param $optionId
     * @return string
     */
    public function getOptionGrid($optionId)
    {
        $options = self::getOptionArray();
        if ($optionId == self::YES) {
            $html = '<span class="grid-severity-notice"><span>' . $options[$optionId] . '</span>' . '</span>';
        }
        if ($optionId == self::NO) {
            $html = '<span class="grid-severity-critical"><span>' . $options[$optionId] . '</span></span>';
        }

        return $html;

    }
}
