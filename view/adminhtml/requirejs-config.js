var config = {
    map: {
        '*': {
            bootstrap: 'Magenest_FAQProfessional/js/bootstrap.min',
            jquery_min: 'Magenest_FAQProfessional/js/jquery.min',
            jquery_ui: 'Magenest_FAQProfessional/js/jquery.ui',
        }
    },
    shim:{
        'varien/js':{
            'deps':['prototype']
        }
    }
};
