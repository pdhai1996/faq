require([
    "jquery",
    'mage/template',
    "mage/mage",
    'jquery/ui'
], function ($) {
    'use strict';
    $(document).ready(function () {
        var title=$(".title");
        var contentIndex=0; // Faq tab being showed
        var content=$(".content-container");// Get all content tab
        $(content[0]).show();
        title.click(function () {
            var faq=$(this);
            var des=faq.parent(".faq-container").children(".description");
            var id=faq.attr("faq-id");
            if(des.is(":visible")){
                des.hide(500);
            } else {
                var url=$("#view-url").attr("view-url");
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        id: id
                    },
                    showLoader: false,
                    success: function (response) {
                        if (!response.error) {
                            // alert("Success");
                        } else {
                            //TODO when have errors
                            // alert('Please check request !');
                        }
                    },
                    timeout: 10000
                });
                des.show(500);
            }
        });

        $("#first").click(function () {
            content.hide(300);
            contentIndex=0;
            $(content[0]).show(300);
        });

        $("#pre").click(function () {
            if(contentIndex>0){
                content.hide(300);
                contentIndex--;
                $(content[contentIndex]).delay(300).show(300);
            }
        });

        $("#next").click(function () {
            if(contentIndex<content.length-1){
                content.hide(300);
                contentIndex++;
                $(content[contentIndex]).delay(300).show(300);
            }
        });
        $("#last").click(function () {
            content.hide(300);
            contentIndex=content.length-1;
            $(content[contentIndex]).delay(300).show(300);
        });

    });
});
