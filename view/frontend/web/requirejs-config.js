var config = {
    map: {
        '*': {
            bootstrap: 'Magenest_FAQProfessional/js/bootstrap.min',
            jquery_min: 'Magenest_FAQProfessional/js/jquery.min',
        }
    },
    shim:{
        'varien/js':{
            'deps':['prototype']
        }
    }
};
