<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 08/05/2017
 * Time: 08:52
 */

namespace Magenest\FAQProfessional\Block\Adminhtml\Faq\Edit;

/**
 * Class Form
 * @package Magenest\FAQProfessional\Block\Adminhtml\Faq\Edit
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
