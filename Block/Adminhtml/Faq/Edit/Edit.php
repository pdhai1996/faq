<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 08/05/2017
 * Time: 08:50
 */

namespace Magenest\FAQProfessional\Block\Adminhtml\Faq\Edit;

/**
 * Class Edit
 * @package Magenest\FAQProfessional\Block\Adminhtml\Faq\Edit
 */
class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    )
    {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Retrieve text for header element depending on loaded post
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        if ($this->_coreRegistry->registry('faq')->getId()) {
            return __("FAQ '%1'", $this->escapeHtml($this->_coreRegistry->registry('faq')->getType()));
        } else {
            return __('FAQ');
        }
    }

    /**
     * Initialize blog post edit block
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'Magenest_FAQProfessional';
        $this->_controller = 'adminhtml_faq';
        parent::_construct();

        $this->buttonList->update('save', 'label', __('Save FAQ'));
    }

    /**
     * Prepare layout
     *
     * @return \Magento\Framework\View\Element\AbstractBlock
     */
    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
}