<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 08/05/2017
 * Time: 08:52
 */

namespace Magenest\FAQProfessional\Block\Adminhtml\Faq\Edit;

/**
 * Class Tabs
 * @package Magenest\FAQProfessional\Block\Adminhtml\Faq\Edit
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('page_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('FAQ'));
    }
}
