<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 08/05/2017
 * Time: 08:53
 */

namespace Magenest\FAQProfessional\Block\Adminhtml\Faq\Edit\Tab;
use Psr\Log\LoggerInterface;

/**
 * Class Main
 * @package Magenest\FAQProfessional\Block\Adminhtml\Faq\Edit\Tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements
    \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $authSession;

    /**
     * @var string
     */
    protected $_template = 'faq/edit.phtml';

    /**
     * @var \Magenest\FAQProfessional\Model\FAQCategoryFactory
     */
    protected $categoryFactory;

    /**
     * @var \Magenest\FAQProfessional\Model\FAQFactory
     */
    protected $faqFactory;

    protected $tagFactory;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * \Magento\Catalog\Helper\Category
     */
    protected $_productCategory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magenest\FAQProfessional\Model\FAQCategoryFactory $categoryFactory,
        \Magenest\FAQProfessional\Model\FAQFactory $faqFactory,
        \Magenest\FAQProfessional\Model\FAQTagFactory $tagFactory,
        LoggerInterface $loggerInterface,
        \Magento\Catalog\Helper\Category $category,
        array $data = []
    )
    {
        $this->_logger=$loggerInterface;
        $this->_productCategory=$category;
        $this->authSession = $authSession;
        $this->_coreRegistry = $registry;
        $this->_formFactory = $formFactory;
        $this->categoryFactory = $categoryFactory;
        $this->faqFactory = $faqFactory;
        $this->tagFactory = $tagFactory;

        parent::__construct($context, $registry, $formFactory);

    }


    /**
     * Return Tab label
     *
     * @return string
     * @api
     */
    public function getTabLabel()
    {
        return __('General');

    }


    /**
     * Return Tab title
     *
     * @return string
     * @api
     */
    public function getTabTitle()
    {
        return __('FAQ');

    }


    /**
     * Can show tab in tabs
     *
     * @return boolean
     * @api
     */
    public function canShowTab()
    {
        return true;

    }


    /**
     * Tab is hidden
     *
     * @return boolean
     * @api
     */
    public function isHidden()
    {
        return false;

    }

    /**
     * @return mixed|null
     */
    public function getFaq()
    {
        $id = $this->getRequest()->getParam('id');
        if($id) {
            $faq = $this->faqFactory->create()
                ->load($id)->getData();
            return $faq;
        }
        return null;
    }

    public function getCategories()
    {
        $data = $this->categoryFactory->create()->getCollection()->getData();
        return $data;
    }

    public function getTags()
    {
        $data = $this->tagFactory->create()->getCollection()->getData();
        return $data;
    }

    public function getAllProductCategory(){
        $categories=$this->_productCategory->getStoreCategories();
        return $categories;
    }

    /**
     * check if this faq is in a product category
     * @param $idCtg
     */
    public function isProductCategorySelected($idCtg){
        $faq=$this->getFaq();
        if(isset($faq['product_category_id'])){
        $ctgString = $faq['product_category_id'];
            if (substr($ctgString, 0, strlen($idCtg)) === $idCtg || strpos($ctgString, ',' . $idCtg . ',')) {
                return 'selected';
            }
        }
        return '';
    }
    
}
