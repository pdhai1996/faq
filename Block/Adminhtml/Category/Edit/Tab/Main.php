<?php
/**
 * Created by PhpStorm.
 * User: duccanh
 * Date: 14/07/2016
 * Time: 23:46
 */
namespace Magenest\FAQProfessional\Block\Adminhtml\Category\Edit\Tab;

/**
 * Class Main
 *
 * @package Magenest\FAQProfessional\Block\Adminhtml\Category\Edit\Tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements
    \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $authSession;

    /**
     * @var string
     */
    protected $_template = 'category/edit.phtml';

    protected $categoryFactory;


    /**
     * \Magento\Catalog\Helper\Category
     */
    protected $_productCategory;
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magenest\FAQProfessional\Model\FAQCategoryFactory $categoryFactory,
        \Magento\Catalog\Helper\Category $productCategory,
        array $data = []
    )
    {
        $this->_productCategory=$productCategory;
        $this->authSession = $authSession;
        $this->_coreRegistry = $registry;
        $this->_formFactory = $formFactory;
        $this->categoryFactory = $categoryFactory;

        parent::__construct($context, $registry, $formFactory);

    }


    /**
     * Return Tab label
     *
     * @return string
     * @api
     */
    public function getTabLabel()
    {
        return __('General');

    }


    /**
     * Return Tab title
     *
     * @return string
     * @api
     */
    public function getTabTitle()
    {
        return __('FAQ Category');

    }


    /**
     * Can show tab in tabs
     *
     * @return boolean
     * @api
     */
    public function canShowTab()
    {
        return true;

    }


    /**
     * Tab is hidden
     *
     * @return boolean
     * @api
     */
    public function isHidden()
    {
        return false;

    }

    public function getCategory()
    {
        $id = $this->getRequest()->getParam('id');
        if($id) {
            $category = $this->categoryFactory->create()
                ->load($id)->getData();
            return $category;
        }
        return null;
    }

    public function getAllProductCategory(){
        $categories=$this->_productCategory->getStoreCategories();
        return $categories;
    }
}
