<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 03/06/2017
 * Time: 09:16
 */
namespace Magenest\FAQProfessional\Block\Category;
use Magento\Framework\View\Element\Template;
use Magento\Framework\Registry;
use Magenest\FAQProfessional\Model\FAQFactory;
use Magenest\FAQProfessional\Model\Url;
use Magenest\FAQProfessional\Model\Config;
use Magenest\FAQProfessional\Model\ResourceModel\FAQ;
class  Sidebar extends Template{

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var FAQFactory
     */
    protected $_faqFactory;

    /**
     * @var FAQ
     */
    protected $_faqCollection;

    const CATEGORY_ENABABLE_CONFIG='faq_category/faq_category_enable/category_on';
    
    const CATEGORY_NUMBER_FAQ_CONFIG='faq_category/frontend_config/number_faq';

    /**
     * Sidebar constructor.
     * @param FAQFactory $faqFactory
     * @param Registry $registry
     * @param Template\Context $context
     * @param FAQ $faqCollection
     * @param array $data
     */
    public function __construct(
        FAQFactory $faqFactory,
        Registry $registry,
        Template\Context $context,
        FAQ $faqCollection,
        array $data)
    {
        $this->_faqCollection=$faqCollection;
        $this->_faqFactory=$faqFactory;
        $this->_coreRegistry=$registry;
        parent::__construct($context, $data);
    }

    /**
     * @return mixed
     */
    public function getCurrenCategory(){
        $category=$this->_coreRegistry->registry('current_category');
        return $category->getId();
    }

    /**
     * @return array
     * Get all question of the catagory
     */
    public function getAllFaq(){
            $data = $this->_faqCollection->getFaqsCategory();
        $id=$this->getCurrenCategory();
        $result=[];
        foreach ($data as $faq){
            $ctgString=$faq['product_category_id'];
            if(substr($ctgString, 0, strlen($id))===$id||strpos($ctgString,','.$id.',')){
                array_push($result,$faq);
            }
        }
        return $result;
    }

    /**
     * @return string
     *
     */
    public function getViewUrl(){
        return $this->getUrl(Url::ROUTE_FAQ_VIEW);
    }

    /**
     * @return mixed
     * Check if the category bar is enable
     */
    public function isCategoryFaqEnable(){
        $value= $this->_scopeConfig->getValue(self::CATEGORY_ENABABLE_CONFIG);
        return $value;
    }

    /**
     * @return int|mixed
     * Get number faq per page in category bar
     */
    public function getFAQConfigNumber(){
        $value=$this->_scopeConfig->getValue(self::CATEGORY_NUMBER_FAQ_CONFIG);
        if(!(is_numeric($value)&&$value>0)){
            return 5;
        }
        return $value;
    }
}