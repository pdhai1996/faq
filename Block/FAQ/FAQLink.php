<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 04/05/2017
 * Time: 10:17
 */

namespace Magenest\FAQProfessional\Block\FAQ;

use Magenest\FAQProfessional\Model\Url;
use Magento\Framework\View\Element\Html\Link;

class FAQLink extends Link
{

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return __('FAQ Professional');
    }

    /**
     * @return string
     */
    public function getHref()
    {
        return $this->getUrl(Url::ROUTE_FAQ_INDEX);
    }
}