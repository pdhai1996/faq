<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 04/05/2017
 * Time: 08:25
 */

namespace Magenest\FAQProfessional\Setup;


use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\SetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $this->createFAQCategoryTable($installer);
        $this->createFAQTagTable($installer);
        $this->createFAQTable($installer);
        $this->createFAQFeatureTable($installer);

        $installer->endSetup();
    }

    public function createFAQCategoryTable($installer)
    {
        $tableName = 'faq_category';
        if ($installer->tableExists($tableName)) {
            $installer->getConnection()->dropTable($tableName);
        }
        $table = $installer->getConnection()->newTable($installer->getTable($tableName))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Category FAQ ID'
            )
            ->addColumn(
                'name',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => ''],
                'Category Name'
            )
            ->addColumn(
                'sort_order',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Sort Order'
            )
            ->addColumn(
                'status', Table::TYPE_SMALLINT,
                5,
                ['nullable' => false, 'default' => '1'],
                '1 - Enabled, 2 - Disabled'
            )
            ->addColumn(
                'create_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                'Create At'
            )
            ->addColumn(
                'update_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                'Update At'
            );
        $installer->getConnection()->createTable($table);
    }

    public function createFAQTagTable($installer)
    {
        $tableName = 'faq_tag';
        if ($installer->tableExists($tableName)) {
            $installer->getConnection()->dropTable($tableName);
        }
        $table = $installer->getConnection()->newTable($installer->getTable($tableName))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'FAQ Tag ID'
            )
            ->addColumn(
                'tag_name',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => ''],
                'Tag Name'
            )
            ->addColumn(
                'create_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                'Create At'
            )
            ->addColumn(
                'update_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                'Update At'
            );
        $installer->getConnection()->createTable($table);
    }

    public function createFAQTable($installer)
    {
        $tableName = 'faq_faq';
        if ($installer->tableExists($tableName)) {
            $installer->getConnection()->dropTable($tableName);
        }
        $table = $installer->getConnection()->newTable($installer->getTable($tableName))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'FAQ ID'
            )
            ->addColumn(
                'title',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => ''],
                'Title'
            )
            ->addColumn(
                'description',
                Table::TYPE_TEXT,
                255,
                [],
                'Description'
            )
            ->addColumn(
                'tags_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '64k',
                ['nullable' => true, 'default' => ''],
                'Tags Name'
            )
            ->addColumn(
                'category_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Category Id'
            )
            ->addColumn(
                'product_category_id',
                Table::TYPE_TEXT,
                null,
                ['unsigined'=>true, 'nullable'=>true],
                'Product Category Id'
            )
            ->addColumn(
                'url_key',
                Table::TYPE_TEXT,
                255,
                [],
                'URL Key'
            )
            ->addColumn(
                'most_frequently',
                Table::TYPE_SMALLINT,
                5,
                ['nullable' => false, 'default' => '2'],
                '1 - Yes, 2 - No'
            )
            ->addColumn(
                'sort_order',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Sort Order'
            )
            ->addColumn(
                'status', Table::TYPE_SMALLINT,
                5,
                ['nullable' => false, 'default' => '1'],
                '1 - Enabled, 2 - Disabled'
            )
            ->addColumn(
                'create_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                'Create At'
            )
            ->addColumn(
                'update_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                'Update At'
            )
            ->addForeignKey(
                $installer->getFkName('faq_faq', 'category_id', 'faq_category', 'id'),
                'category_id',
                $installer->getTable('faq_category'),
                'id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            );
        $installer->getConnection()->createTable($table);
    }

    /**
     * @param $installer
     * This table contain the view of any question
     */
    public function createFAQFeatureTable($installer)
    {
        $tableName = 'faq_feature';
        if ($installer->tableExists($tableName)) {
            $installer->getConnection()->dropTable($tableName);
        }
        $table = $installer->getConnection()->newTable($installer->getTable($tableName))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'primary' => true],
                'FAQ ID'
            )
            ->addColumn(
                'number_of_view',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Number Of View'
            )
            ->addColumn(
                'create_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                'Create At'
            )
            ->addColumn(
                'update_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                'Update At'
            )
            ->addForeignKey(
                $installer->getFkName('faq_feature', 'id', 'faq_faq', 'id'),
                'id',
                $installer->getTable('faq_faq'),
                'id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            );
        $installer->getConnection()->createTable($table);
    }
}