##CHANGELOG - FAQ Professional
=============
##1.0.1 - 09/06/2017
=============
- Create FAQs
- Create your own FAQ categories and tags
- Show customer FAQs by Category and Tag in FAQ page
- Sort FAQ by views or sort order
- Show FAQs in category page
- Config FAQs frontend