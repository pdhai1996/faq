<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 04/05/2017
 * Time: 10:49
 */

namespace Magenest\FAQProfessional\Controller\FAQ;

use Magento\Framework\App\Action\Action;

class Index extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\UrlRewrite\Model\UrlRewriteFactory
     */
    protected $_urlRewrite;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_store;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewrite,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    )
    {
        $this->_store=$storeManagerInterface;
        $this->_urlRewrite=$urlRewrite;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $this->addUrlRewrite();
        $result=$this->resultPageFactory->create();
        $result->getConfig()->getTitle()->set('FAQ');
        return $result;
    }

    public function addUrlRewrite(){
        $model=$this->_urlRewrite->create();
        $id=$this->_store->getStore()->getId();
        $collection=$model->getCollection()->addFieldToFilter('target_path','faq/faq/index')
        ->addFieldToFilter('store_id',$id);
        if(count($collection)==0) {
            $page = [];
            $page['url_rewrite_id'] = null;
            $page['request_path'] = 'FAQ';
            $page['target_path'] = 'faq/faq/index';
            $page['store_id']       = $id;
            $model->setData($page);
            $model->save();
        }
    }
}