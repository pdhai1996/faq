<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 20/05/2017
 * Time: 15:39
 */
namespace Magenest\FAQProfessional\Controller\FAQ;
use Magento\Framework\App\Action\Action;
use Magenest\FAQProfessional\Model\FAQFeatureFactory;
use Magento\Framework\App\Action\Context;
use Psr\Log\LoggerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Magenest\FAQProfessional\Model\ResourceModel\FAQFeature;

class IncreaseView extends Action{

    /**
     * @var FAQFeatureFactory
     */
    protected $_featureFactory;

    protected $_logger;

    protected  $_featureCollection;
    public function __construct(
        LoggerInterface $loggerInterface,
        FAQFeatureFactory $featureFactory,
        FAQFeature $featureCollection,
        Context $context)
    {
        $this->_logger=$loggerInterface;
        $this->_featureFactory=$featureFactory;
        $this->_featureCollection=$featureCollection;
        parent::__construct($context);
    }

    public function execute()
    {   try {
        $value = $this->getRequest()->getPostValue();
        $id = $value['id'];
        $model = $this->_featureFactory->create()->load($id);
        if (!$model->getCreateAt()) {
            $data = [
                'id' => $id
            ];
            $this->_featureCollection->insertData($data);
        }
        $model = $this->_featureFactory->create()->load($id);
        $view = (int)$model->getNumberOfView();
        $view++;
        $model->setNumberOfView($view);
        $model->save();
    }catch (\Exception $e){
        $this->_logger->debug($e->getMessage());
    }
        // TODO: Implement execute() method.
    }
}