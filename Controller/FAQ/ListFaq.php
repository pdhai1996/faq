<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 11/05/2017
 * Time: 08:15
 */

namespace Magenest\FAQProfessional\Controller\FAQ;

use Magento\Framework\Controller\Result\JsonFactory as ResultJsonFactory;
use Magenest\FAQProfessional\Model\FAQ;
use Magento\Framework\App\Action\Action;
use Magenest\FAQProfessional\Model\ResourceModel\FAQ as FAQColection;

class ListFaq extends Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var ResultJsonFactory
     */
    protected $resultJsonFactory;
    
    protected $faqFactory;

    protected $_logger;

    protected $_faqCollection;

    /**
     * Index constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry,
        ResultJsonFactory $resultJsonFactory,
        \Magenest\FAQProfessional\Model\FAQFactory $faqFactory,
        \Psr\Log\LoggerInterface $logger,
        FAQColection $faqCollection
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->registry = $registry;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->faqFactory = $faqFactory;
        $this->_logger = $logger;
        $this->_faqCollection=$faqCollection;
    }

    public function execute()
    {
        $data = $this->_getDataJson();
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($data);
    }

    /**
     * Get Data Json
     *
     * @return array
     */
    public function _getDataJson()
    {
        $data = $this->getRequest()->getPostValue();

        $description = 'Done';
        $error = true;
        $title = '';
        try {
            $faqs = $this->_faqCollection->getFaqs($data);
            $error=false;
        } catch (\Exception $e){
        }
//        if (isset($data['keyword'])) {
//            $keyword = $data['keyword'];
//            $listFaqs = $this->faqFactory->create()->getCollection()
//                ->addFieldToFilter('status', FAQ::STATUS_ENABLED)
//                ->setOrder('sort_order', 'DESC')
//                ->getData();
//            $faqs = [];
//            foreach ($listFaqs as $key => $faq) {
//                if(strpos($faq['title'], $keyword) !== false || strpos($faq['description'], $keyword) !== false) {
//                    $faqs[] = $faq;
//                }
//            }
//            $error = false;
//        }
//
//        if (isset($data['most_frequently'])) {
//            $faqs = $this->faqFactory->create()->getCollection()
//                ->addFieldToFilter('status', FAQ::STATUS_ENABLED)
//                ->addFieldToFilter('most_frequently', FAQ::MOST_FREQUENTLY_YES)
//                ->setOrder('sort_order', 'DESC')
//                ->getData();
//            $error = false;
//        }
//
//        if (isset($data['cate_id'])) {
//            $faqs = $this->faqFactory->create()->getCollection()
//                ->addFieldToFilter('status', FAQ::STATUS_ENABLED)
//                ->addFieldToFilter('category_id', $data['cate_id'])
//                ->setOrder('sort_order', 'DESC')
//                ->getData();
//            $error = false;
//        }
//
//        if (isset($data['tag_name'])) {
//            $listFaqs = $this->faqFactory->create()->getCollection()
//                ->addFieldToFilter('status', FAQ::STATUS_ENABLED)
//                ->setOrder('sort_order', 'DESC')
//                ->getData();
//            $faqs = [];
//            foreach($listFaqs as $key => $faq) {
//                if($data['tag_name'] == $faq['tags_name']){
//                    $faqs[] = $faq;
//                } else {
//                    $arrTagName = explode(',', $faq['tags_name']);
//                    foreach ($arrTagName as $item) {
//                        $item = trim($item);
//                        if($item == $data['tag_name']){
//                            $faqs[] = $faq;
//                        }
//                    }
//                }
//            }
//
//            $error = false;
//        }

        $data = [
            'description' => $description,
            'error' => $error,
            'faqs' => $faqs
        ];

        return $data;
    }
}