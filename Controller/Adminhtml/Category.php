<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 05/05/2017
 * Time: 08:03
 */

namespace Magenest\FAQProfessional\Controller\Adminhtml;

use Magento\Framework\App\Action\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Ui\Component\MassAction\Filter;

/**
 * Class Category
 * @package Magenest\FAQProfessional\Controller\Adminhtml
 */
abstract class Category extends Action
{
    /**
     * @var \Magento\Framework\Registry|null
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $resultRawFactory;

    /**
     * @var \Magento\UrlRewrite\Model\UrlRewrite
     */
    protected $_urlRewrite;

    /**
     * @var \Magento\Framework\View\LayoutFactory
     */
    protected $layoutFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * @var \Magenest\FAQProfessional\Model\FAQCategoryFactory
     */
    protected $categoryFactory;

    /**
     * @var \Magenest\FAQProfessional\Model\FAQFactory
     */
    protected $faqFactory;

    /**
     * @var Filter
     */
    protected $_filter;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_timezone;

    /**
     * Category constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param PageFactory $resultPageFactory
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     * @param \Magento\Framework\View\LayoutFactory $layoutFactory
     * @param Filter $filter
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param \Magenest\FAQProfessional\Model\FAQCategoryFactory $categoryFactory
     * @param \Magenest\FAQProfessional\Model\FAQFactory $faqFactory
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $datetime
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        Filter $filter,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magenest\FAQProfessional\Model\FAQCategoryFactory $categoryFactory,
        \Magenest\FAQProfessional\Model\FAQFactory $faqFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $datetime
    )
    {
        $this->_context = $context;
        $this->_coreRegistry = $coreRegistry;
        $this->_resultPageFactory = $resultPageFactory;
        $this->resultRawFactory = $resultRawFactory;
        $this->layoutFactory = $layoutFactory;
        $this->_filter = $filter;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->categoryFactory = $categoryFactory;
        $this->faqFactory = $faqFactory;
        $this->_timezone = $datetime;
        parent::__construct($context);

    }


    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->_resultPageFactory->create();

        return $resultPage;

    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_FAQProfessional::category');
    }
}