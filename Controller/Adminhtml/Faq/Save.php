<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 09/05/2017
 * Time: 15:16
 */

namespace Magenest\FAQProfessional\Controller\Adminhtml\Faq;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Psr\Log\LoggerInterface;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class Save
 * @package Magenest\FAQProfessional\Controller\Adminhtml\Faq
 */
class Save extends Action
{
    /**
     * @var \Magenest\FAQProfessional\Model\FAQCategoryFactory
     */
    protected $categoryFactory;

    /**
     * @var \Magenest\FAQProfessional\Model\FAQFactory
     */
    protected $faqFactory;

    /**
     * @var \Magenest\FAQProfessional\Model\FAQTagFactory
     */
    protected $faqTagFactory;

    protected $faqFeatureFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_timezone;

    /**
     * @var LoggerInterface
     */
    protected $_logger;
    /**
     * @param Context $context
     */
    public function __construct(
        Context $context,
        LoggerInterface $loggerInterface,
        \Magenest\FAQProfessional\Model\FAQCategoryFactory $categoryFactory,
        \Magenest\FAQProfessional\Model\FAQFactory $faqFactory,
        \Magenest\FAQProfessional\Model\FAQTagFactory $faqTagFactory,
        \Magenest\FAQProfessional\Model\FAQFeatureFactory $faqFeatureFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $datetime
    )
    {
        parent::__construct($context);
        $this->_logger=$loggerInterface;
        $this->categoryFactory = $categoryFactory;
        $this->faqFactory = $faqFactory;
        $this->faqTagFactory = $faqTagFactory;
        $this->faqFeatureFactory = $faqFeatureFactory;
        $this->_timezone = $datetime;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_FAQProfessional::save');
    }

    /**
     * @return $this
     */
    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $data = $this->getRequest()->getPostValue();
        $productCtg=$data['product_category'];
        $categoryString='';
        foreach ($productCtg as $ctg){
            $categoryString.=$ctg;
            $categoryString.=',';
        }
        if($data) {
            $checkNull = false;
            foreach ($data as $key => $value) {
                if(!isset($value)&&$value!='' && $key != 'id') {
                    $this->_logger->debug($key);
                    $checkNull = true;

                }
            }
            if(!$checkNull) {
                try{
                    $id = $this->getRequest()->getParam('id');
                    if($id) {
                        $tags = explode(',', $data['tags_name']);
                        foreach ($tags as $item) {
                            $item = trim($item);
                            $tag = $this->faqTagFactory->create()->getCollection()
                                ->addFieldToFilter('tag_name', $item)
                                ->getData();
                            if(!$tag) {
                                $tagCollection = $this->faqTagFactory->create();
                                $tagCollection->setData([
                                    'tag_name' => $item
                                ]);
                                $tagCollection->save();
                            }
                        }
                        $faq = $this->faqFactory->create()->load($data['id']);
                        $faq->setDescription($data['description'])->save();
                        $faq->setTagsName($data['tags_name'])->save();
                        $faq->setCategoryId($data['category_id'])->save();
                        $faq->setUrlKey($data['url_key'])->save();
                        $faq->setMostFrequently($data['most_frequently'])->save();
                        $faq->setSortOrder($data['sort_order'])->save();
                        $faq->setStatus($data['status'])->save();
                        $faq->setProductCategoryId($categoryString)->save();
                    } else {
                        $faq = $this->faqFactory->create()->getCollection()
                            ->addFieldToFilter('title', $data['title'])
                            ->getData();
                        if($faq) {
                            $this->messageManager->addErrorMessage(__('This faq title has been existed.'));
//                            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
                        }
                        $tags = explode(',', $data['tags_name']);
                        foreach ($tags as $item) {
                            $item = trim($item);
                            $tag = $this->faqTagFactory->create()->getCollection()
                                ->addFieldToFilter('tag_name', $item)
                                ->getData();
                            if(!$tag) {
                                $tagCollection = $this->faqTagFactory->create();
                                $tagCollection->setData([
                                    'tag_name' => $item
                                ]);
                                $tagCollection->save();
                            }
                        }
                        $collection = $this->faqFactory->create();
                        $collection->setData([
                            'title' => $data['title'],
                            'description' => $data['description'],
                            'tags_name' => $data['tags_name'],
                            'category_id' => $data['category_id'],
                            'url_key' => $data['url_key'],
                            'most_frequently' => $data['most_frequently'],
                            'sort_order' => $data['sort_order'],
                            'status' => $data['status'],
                            'product_category_id'=>$categoryString
                        ]);
                        $collection->save();
//                        $newFaq = $this->faqFactory->create()->getCollection()
//                            ->getLastItem()->getData();
//                        $featureCollection = $this->faqFeatureFactory->create();
//                        $featureCollection->setData([
//                            'id' => $newFaq['id'],
//                            'number_of_view' => 0
//                        ]);
//                        $featureCollection->save();
                        $this->messageManager->addSuccess(__('The FAQ has been saved.'));
                        $resultRedirect->setUrl($this->getUrl('faq/faq/edit',['id'=>$collection->getId()]));
                    }
                    $this->messageManager->addSuccess(__('The FAQ has been saved.'));
//                    return $resultRedirect->setUrl($this->_redirect->getRefererUrl());
                } catch (\Exception $e) {
                    $this->_logger->debug($e->getMessage());
                    $this->messageManager->addError($e, __('Something went wrong while saving the mapping.'));
//                    $resultRedirect->setUrl($this->_redirect->getRefererUrl());
                }
            }
        }

        // Your code
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }
}