<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 06/05/2017
 * Time: 09:46
 */

namespace Magenest\FAQProfessional\Controller\Adminhtml\Faq;

use Magenest\FAQProfessional\Controller\Adminhtml\Faq;

class Index extends Faq
{
    public function execute()
    {
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Magenest_FAQProfessional::faq');
        $resultPage->addBreadcrumb(__('FAQ'), __('FAQ'));
        $resultPage->addBreadcrumb(__('Manage FAQ'), __('Manage FAQ'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage FAQ'));
        return $resultPage;

    }
}