<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 09/05/2017
 * Time: 16:24
 */

namespace Magenest\FAQProfessional\Controller\Adminhtml\Faq;

use Magenest\FAQProfessional\Controller\Adminhtml\Faq;

/**
 * Class MassStatus
 * @package Magenest\FAQProfessional\Controller\Adminhtml\Faq
 */
class MassStatus extends Faq
{
    /**
     * @return $this
     */
    public function execute()
    {
        $faqCollection = $this->faqFactory->create()->getCollection();
        $collections = $this->_filter->getCollection($faqCollection);
        $status = (int)$this->getRequest()->getParam('status');
        $totals = 0;
        try {

            foreach ($collections as $item) {

                $item->setStatus($status)->save();

                $totals++;
            }

            $this->messageManager->addSuccess(__('A total of %1 record(s) have been updated.', $totals));
        } catch (LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*');
    }
}