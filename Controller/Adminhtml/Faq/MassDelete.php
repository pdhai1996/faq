<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 09/05/2017
 * Time: 16:27
 */

namespace Magenest\FAQProfessional\Controller\Adminhtml\Faq;

use Magenest\FAQProfessional\Controller\Adminhtml\Faq;

/**
 * Class MassDelete
 * @package Magenest\FAQProfessional\Controller\Adminhtml\Faq
 */
class MassDelete extends Faq
{
    /**
     * @return $this
     */
    public function execute()
    {
        $faqCollection = $this->faqFactory->create()->getCollection();
        $collections = $this->_filter->getCollection($faqCollection);
        $totals = 0;
        try {

            foreach ($collections as $item) {

                $item->delete();

                $totals++;
            }

            $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', $totals));
        } catch (LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*');
    }
}