<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 06/05/2017
 * Time: 15:17
 */

namespace Magenest\FAQProfessional\Controller\Adminhtml\Category;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;

class Save extends Action
{
    protected $categoryFactory;

    protected $_timezone;

    /**
     * @param Context $context
     */
    public function __construct(
        Context $context,
        \Magenest\FAQProfessional\Model\FAQCategoryFactory $categoryFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $datetime
    )
    {
        parent::__construct($context);
        $this->categoryFactory = $categoryFactory;
        $this->_timezone = $datetime;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_FAQProfessional::save');
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if($data) {
            try{
                $id = $this->getRequest()->getParam('id');
                if($id) {
                    $cate = $this->categoryFactory->create()->load($data['id']);
                    $cate->setSortOrder($data['sort_order'])->save();
                    $cate->setStatus($data['status'])->save();
                    $cate->setName($data['name'])->save();
                } else {
                    $cate = $this->categoryFactory->create()->getCollection()
                        ->addFieldToFilter('name', $data['name'])
                        ->getData();
                    if($cate) {
                        $this->messageManager->addErrorMessage(__('This category name has been existed.'));
                        return $resultRedirect->setPath('faq/category/edit');
                    }
                    $collection = $this->categoryFactory->create();
                    $collection->setData([
                        'name' => $data['name'],
                        'sort_order' => $data['sort_order'],
                        'status' => $data['status']
                    ]);
                    $collection->save();
                }
                $this->messageManager->addSuccess(__('The category has been saved.'));
                return $resultRedirect->setPath('faq/category/index');
            } catch (\Exception $e) {
                $this->messageManager->addError($e, __('Something went wrong while saving the mapping.'));
                return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
            }
        }
        return $resultRedirect->setPath('*/*/');
    }
}