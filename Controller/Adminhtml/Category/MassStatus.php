<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 08/05/2017
 * Time: 08:01
 */

namespace Magenest\FAQProfessional\Controller\Adminhtml\Category;


use Magenest\FAQProfessional\Controller\Adminhtml\Category;

/**
 * Class MassStatus
 * @package Magenest\FAQProfessional\Controller\Adminhtml\Category
 */
class MassStatus extends Category
{
    /**
     * @return $this
     */
    public function execute()
    {
        $categoryCollection = $this->categoryFactory->create()->getCollection();
        $collections = $this->_filter->getCollection($categoryCollection);
        $status = (int)$this->getRequest()->getParam('status');
        $totals = 0;
        try {

            foreach ($collections as $item) {

                $item->setStatus($status)->save();

                $totals++;
            }

            $this->messageManager->addSuccess(__('A total of %1 record(s) have been updated.', $totals));
        } catch (LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*');
    }
}