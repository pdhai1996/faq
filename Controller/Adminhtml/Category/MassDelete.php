<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 08/05/2017
 * Time: 08:01
 */

namespace Magenest\FAQProfessional\Controller\Adminhtml\Category;


use Magenest\FAQProfessional\Controller\Adminhtml\Category;

/**
 * Class MassDelete
 * @package Magenest\FAQProfessional\Controller\Adminhtml\Category
 */
class MassDelete extends Category
{
    /**
     * @return $this
     */
    public function execute()
    {
        $categoryCollection = $this->categoryFactory->create()->getCollection();
        $collections = $this->_filter->getCollection($categoryCollection);
        $totals = 0;
        try {

            foreach ($collections as $item) {
                $cate = $item->getData();
                $faqs = $this->faqFactory->create()->getCollection()
                    ->addFieldToFilter('category_id', $cate['id'])
                    ->getData();
                if(!$faqs) {
                    $item->delete();
                    $totals++;
                }
            }
            ($totals > 0) ? $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', $totals))
                : $this->messageManager->addSuccess(__('Cannot delete any records.'));
        } catch (LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*');
    }
}