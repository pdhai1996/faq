<?php
/**
 * Created by PhpStorm.
 * User: duccanh
 * Date: 18/07/2016
 * Time: 16:53
 */
namespace Magenest\FAQProfessional\Controller\Adminhtml\Category;

use Magenest\FAQProfessional\Controller\Adminhtml\Category;

/**
 * Class Index
 * @package Magenest\FAQProfessional\Controller\Adminhtml\Category
 */
class Index extends Category
{
    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Magenest_FAQProfessional::category');
        $resultPage->addBreadcrumb(__('FAQ'), __('FAQ'));
        $resultPage->addBreadcrumb(__('Manage Category FAQ'), __('Manage Category FAQ'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Category FAQ'));
        return $resultPage;

    }
}
