<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 04/05/2017
 * Time: 09:33
 */

namespace Magenest\FAQProfessional\Model;


use Magento\Framework\Model\AbstractModel;

/**
 * Class FAQ
 * @package Magenest\FAQProfessional\Model
 */
class FAQ extends AbstractModel
{
    /**
     * most_frequently is yes
     *
     * @var most_frequently
     */
    const MOST_FREQUENTLY_YES = 1;

    /**
     * most_frequently is no
     *
     * @var most_frequently
     */
    const MOST_FREQUENTLY_NO = 2;
    
    /**
     * Status enabled
     *
     * @var status
     */
    const STATUS_ENABLED = 1;

    /**
     * Status disabled
     *
     * @var status
     */
    const STATUS_DISABLED = 2;
    
    /**
     *
     */
    public function _construct()
    {
        $this->_init('Magenest\FAQProfessional\Model\ResourceModel\FAQ');
        $this->setIdFieldName('id');
    }
}