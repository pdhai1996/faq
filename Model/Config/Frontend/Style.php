<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 02/06/2017
 * Time: 08:40
 */
namespace Magenest\FAQProfessional\Model\Config\Frontend;
class Style implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => '1',
                'label' => __('Default'),
            ],
            [
                'value' => '2',
                'label' => __('Design'),
            ],
        ];
    }
}
