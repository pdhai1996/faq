<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 02/06/2017
 * Time: 08:40
 */
namespace Magenest\FAQProfessional\Model\Config\Source;
class Sort implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 'sort_order',
                'label' => __('Sort Order'),
            ],
            [
                'value' => 'number_of_view',
                'label' => __('View'),
            ],
        ];
    }
}
