<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 04/05/2017
 * Time: 09:17
 */

namespace Magenest\FAQProfessional\Model;


use Magento\Framework\Model\AbstractModel;

/**
 * Class FAQCategory
 * @package Magenest\FAQProfessional\Model
 */
class FAQCategory extends AbstractModel
{

    /**
     * Status enabled
     * 
     * @var status
     */
    const STATUS_ENABLED = 1;

    /**
     * Status disabled
     *
     * @var status
     */
    const STATUS_DISABLED = 2;
    
    /**
     *
     */
    public function _construct()
    {
        $this->_init('Magenest\FAQProfessional\Model\ResourceModel\FAQCategory');
        $this->setIdFieldName('id');
    }
}