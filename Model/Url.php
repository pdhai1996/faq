<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 04/05/2017
 * Time: 10:43
 */

namespace Magenest\FAQProfessional\Model;


class Url
{
    const ROUTE_FAQ_INDEX = 'faq/faq';
    
    const ROUTE_FAQ_LIST = 'faq/faq/listfaq';

    const ROUTE_FAQ_VIEW = 'faq/faq/increaseview';
}