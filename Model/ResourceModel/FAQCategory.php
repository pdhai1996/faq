<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 04/05/2017
 * Time: 09:21
 */

namespace Magenest\FAQProfessional\Model\ResourceModel;


use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class FAQCategory
 * @package Magenest\FAQProfessional\Model\ResourceModel
 */
class FAQCategory extends AbstractDb
{
    /**
     * Status enabled
     *
     * @var status
     */
    const STATUS_ENABLED = 1;

    /**
     * Status disabled
     *
     * @var status
     */
    const STATUS_DISABLED = 2;

    /**
     * FAQCategory constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param null $connectionName
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        $connectionName = null
    )
    {

        parent::__construct($context, $connectionName);
    }

    /**
     * Construct
     */
    public function _construct()
    {
        $this->_init('faq_category', 'id');
    }
}