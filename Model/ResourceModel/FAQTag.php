<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 04/05/2017
 * Time: 09:29
 */

namespace Magenest\FAQProfessional\Model\ResourceModel;


use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class FAQTag
 * @package Magenest\FAQProfessional\Model\ResourceModel
 */
class FAQTag extends AbstractDb
{
    /**
     * FAQTag constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param null $connectionName
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        $connectionName = null
    )
    {

        parent::__construct($context, $connectionName);
    }

    /**
     * Construct
     */
    public function _construct()
    {
        $this->_init('faq_tag', 'id');
    }
}