<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 04/05/2017
 * Time: 09:37
 */

namespace Magenest\FAQProfessional\Model\ResourceModel\FAQFeature;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Magenest\FAQProfessional\Model\ResourceModel\FAQFeature
 */
class Collection extends AbstractCollection
{
    /**
     * @var int
     */
    protected $_idFieldName = 'id';

    /**
     *  Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Magenest\FAQProfessional\Model\FAQFeature', 'Magenest\FAQProfessional\Model\ResourceModel\FAQFeature');

    }
}