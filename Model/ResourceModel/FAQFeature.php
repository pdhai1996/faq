<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 04/05/2017
 * Time: 09:36
 */

namespace Magenest\FAQProfessional\Model\ResourceModel;


use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class FAQFeature
 * @package Magenest\FAQProfessional\Model\ResourceModel
 */
class FAQFeature extends AbstractDb
{
    /**
     * FAQFeature constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param null $connectionName
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        $connectionName = null
    )
    {

        parent::__construct($context, $connectionName);
    }

    /**
     * Construct
     */
    public function _construct()
    {
        $this->_init('faq_feature', 'id');
    }

    public function insertData($data){
        $table = $this->getTable($this->getConnection()->getTableName('faq_feature'));
        $this->getConnection()->insert($table, $data);
    }
}