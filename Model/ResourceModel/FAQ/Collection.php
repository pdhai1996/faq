<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 04/05/2017
 * Time: 09:34
 */

namespace Magenest\FAQProfessional\Model\ResourceModel\FAQ;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Magenest\FAQProfessional\Model\ResourceModel\FAQ
 */
class Collection extends AbstractCollection
{
    /**
     * @var int
     */
    protected $_idFieldName = 'id';

    /**
     *  Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Magenest\FAQProfessional\Model\FAQ', 'Magenest\FAQProfessional\Model\ResourceModel\FAQ');

    }
}