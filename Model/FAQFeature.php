<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 04/05/2017
 * Time: 09:35
 */

namespace Magenest\FAQProfessional\Model;


use Magento\Framework\Model\AbstractModel;

/**
 * Class FAQFeature
 * @package Magenest\FAQProfessional\Model
 */
class FAQFeature extends AbstractModel
{
    /**
     *
     */
    public function _construct()
    {
        $this->_init('Magenest\FAQProfessional\Model\ResourceModel\FAQFeature');
        $this->setIdFieldName('id');
    }
}