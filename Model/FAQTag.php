<?php
/**
 * Created by PhpStorm.
 * User: naq259
 * Date: 04/05/2017
 * Time: 09:28
 */

namespace Magenest\FAQProfessional\Model;


use Magento\Framework\Model\AbstractModel;

/**
 * Class FAQTag
 * @package Magenest\FAQProfessional\Model
 */
class FAQTag extends AbstractModel
{
    /**
     *
     */
    public function _construct()
    {
        $this->_init('Magenest\FAQProfessional\Model\ResourceModel\FAQTag');
        $this->setIdFieldName('id');
    }
}